﻿<%@  language="VBScript" %>

<%
    Response.Expires=-1
    Response.CacheControl="no-cache"
%>

<!--#include file="MasterPages/header.asp"-->
<!--#include file="Modules/Contact/ContactList.asp"-->
<!--#include file="Modules/Contact/ContactModel.asp"-->
<!--#include file="Modules/Contact/UserConcernModel.asp"-->

<%
    Dim ConcernSuccess 


    If (Request.Form.Count  > 0) Then 
            
        Dim UserConcern   

        Set UserConcern = new UserConcernModel
       
        UserConcern.UserId = Cstr(Request.Form("Id"))
        UserConcern.ConcernId =Request.Form.Item("concern")
        UserConcern.Details = Request.Form("details")  

        If CreateConcern(UserConcern) = 1 Then 
            
            ConcernSuccess = 1

        End If

    End If
%>
<%

    Dim concerns 
    Dim Username

    Set concerns = GetConcernTypes

    If Not (IsEmpty(Session("Username"))) Then 
        Username = Session("Username")
    Else
        Username = "Guest"
    End If

    
%>
<div id="page-content-wrapper">
    
    <div class="container-fluid">
        
<div class="row">

            <div>
            <h3>Need Assistance</h3>
              <p>  Please describe your problem below. Thank you. </p>
            </div>
</div>

        <div class="row">

            
            <form action="<%=Request.ServerVariables("URL")%>" method="post">


                <input type="hidden" name="Id" value="<%=Session("Id")%>"/>
                <div class="form-group">
                    <label class="control-label" for="username">Username:</label>
                    <input type="text" class="form-control" disabled     id="username"  name="username" value=<%=Username%> />
                </div>


                <div class="form-group">
                    <label class="control-label" for="concern">Concern:</label>
                    <select id="concern" name="concern" class="form-control">
                        <option id="">--SELECT--</option>
                        <%For Each concern In concerns %>
                        <option value="<%=concern.Id%>"><%=concern.TypeName%></option>
                        <%Next %>
                    </select>
                </div>

                <div class="form-group">

                    <label class="control-label" for="details">Details:</label>
                    <textarea class="form-control" name="details" id="details"></textarea>
                </div>
                <div>
                    <input type="submit" value="Submit"/>
                </div>
            </form>

        </div>
    
    </div>

</div>

<!--#include file="MasterPages/footer.html"-->

<script>

    $(function () {

        var concernSuccess = Boolean(<%=Cstr(ConcernSuccess)%>)

        if(concernSuccess === true){

            bootbox.alert("We have received your concern(s) please wait our reply after 24-48 hours");
        }

    });

</script>