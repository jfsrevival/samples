﻿<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="Survey.asp">XCompany Survey</a>
        </li>
        
        <%If  IsEmpty(Session("LoggedIn")) Then%>
        <li>
            <a href="Registration.asp">Register</a>
        </li>
        <li>
            <a href="Login.asp" >LogIn</a>
        </li>

        <%Else%>

        <li>
            <a id="logout" href="#">Log out</a>
        </li>

        <%End If%>

        <li>
            <a href="#">About</a>
        </li>
        <li>
            <a href="ContactUs.asp">Contact Us</a>
        </li>
    </ul>
</div>

