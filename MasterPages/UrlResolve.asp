﻿<%
Function UrlContent(sUrl)
    If InStr(1, sUrl, "~/") = 1 Then
        UrlContent = ApplicationPath & Mid(sUrl, 2)
    Else
        UrlContent = sUrl
    End If
End Function

Function ApplicationPath()

    Dim pos: pos = Len(Request.ServerVariables("INSTANCE_META_PATH")) + 6

    ApplicationPath = Mid(Request.ServerVariables("APPL_MD_PATH"), pos)

End Function
%>