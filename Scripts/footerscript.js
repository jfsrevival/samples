﻿

    $("a#logout").click(function (e) {

        var logOutMessage = "Do you really want to logout on this page?";

        bootbox.confirm(logOutMessage, function (result) {

            if (result === true) {

                var dialog = bootbox.dialog({
                    message: '<p class="text-center"><img src="/images/ajax-loader.gif"/> Please wait while we end your session...</p>',
                    closeButton: false
                });

                setTimeout(function () {
                    window.location.href = "Logout.asp";
                },500);
                
            }
        });
    });