<%


Class UserConcernModel

Private m_UserId 
Public Property Get UserId 
    UserId = m_UserId
End Property

Public Property Let UserId(value)
    m_UserId = value 
End Property 

Private m_ConcernId 
Public Property Get ConcernId 
    ConcernId = m_ConcernId
End Property

Public Property Let ConcernId(value)
    m_ConcernId = value 
End Property 


Private m_Details 
Public Property Get Details 
    Details = m_Details
End Property

Public Property Let Details(value)
    m_Details = value 
End Property 


End Class


%>
