﻿<%
 
    Dim Connection
    Dim Command
    Dim RecordSet
    
    Function CreateConcern(userconcern)

        Dim AffectedRows 

        Set Connection = Server.CreateObject("ADODB.Connection")

        Connection.ConnectionString = "Provider=SQLOLEDB.1;Data Source=.\SQLEXPRESS;Initial Catalog=SimpleBlogDb;user id =sa;password=p@ssw0rd"

        Set Command = Server.CreateObject("ADODB.Command") 

        

        Connection.Open

        With Command 
            .ActiveConnection = Connection
            .CommandText = "usp_CreateNew_Concerns"
            .CommandType = 4
        End With


        Command.Parameters("@UserId") = userconcern.UserId
        Command.Parameters("@ConcernId") = userconcern.ConcernId
        Command.Parameters("@Details") = userconcern.Details

        Command.Execute AffectedRows    
        
        CreateConcern = AffectedRows

        Set Command = Nothing

        Connection.Close

        Set Connection = Nothing


    End Function


    Function GetConcernTypes
        
        Dim ConcernList 

        Set ConcernList = CreateObject("System.Collections.ArrayList") 

        Set Connection = Server.CreateObject("ADODB.Connection")

        Connection.ConnectionString = "Provider=SQLOLEDB.1;Data Source=.\SQLEXPRESS;Initial Catalog=SimpleBlogDb;user id =sa;password=p@ssw0rd"

        Set Command = Server.CreateObject("ADODB.Command")

        Set RecordSet = Server.CreateObject("ADODB.RecordSet")
        
        Connection.Open

        With Command
            .ActiveConnection = Connection
            .CommandText = "srp_GetAllConcernsType"
            .CommandType = 4
        End With

        Set RecordSet = Command.Execute
        
        Set Command = Nothing

        If (Not RecordSet.EOF) Then 
            
            RecordSet.MoveFirst

            Do Until RecordSet.EOF = True
            
                Dim row 
                
                Set row = new ContactList

                row.Id = RecordSet(0)
                
                row.TypeName = RecordSet(1)

                ConcernList.Add row
                 
                RecordSet.MoveNext 
            Loop

        End If
         
        Set GetConcernTypes = ConcernList
        
        RecordSet.Close
   
        Connection.Close
        
    End Function
 %>