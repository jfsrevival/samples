USE [master]
GO
/****** Object:  Database [SimpleBlogDb]    Script Date: 12/14/2016 18:09:07 ******/
CREATE DATABASE [SimpleBlogDb] ON  PRIMARY 
( NAME = N'SimpleBlogDb', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\SimpleBlogDb.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SimpleBlogDb_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\SimpleBlogDb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SimpleBlogDb] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SimpleBlogDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SimpleBlogDb] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [SimpleBlogDb] SET ANSI_NULLS OFF
GO
ALTER DATABASE [SimpleBlogDb] SET ANSI_PADDING OFF
GO
ALTER DATABASE [SimpleBlogDb] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [SimpleBlogDb] SET ARITHABORT OFF
GO
ALTER DATABASE [SimpleBlogDb] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [SimpleBlogDb] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [SimpleBlogDb] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [SimpleBlogDb] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [SimpleBlogDb] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [SimpleBlogDb] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [SimpleBlogDb] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [SimpleBlogDb] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [SimpleBlogDb] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [SimpleBlogDb] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [SimpleBlogDb] SET  DISABLE_BROKER
GO
ALTER DATABASE [SimpleBlogDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [SimpleBlogDb] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [SimpleBlogDb] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [SimpleBlogDb] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [SimpleBlogDb] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [SimpleBlogDb] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [SimpleBlogDb] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [SimpleBlogDb] SET  READ_WRITE
GO
ALTER DATABASE [SimpleBlogDb] SET RECOVERY SIMPLE
GO
ALTER DATABASE [SimpleBlogDb] SET  MULTI_USER
GO
ALTER DATABASE [SimpleBlogDb] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [SimpleBlogDb] SET DB_CHAINING OFF
GO
USE [SimpleBlogDb]
GO
/****** Object:  Table [dbo].[UserAccessLevel]    Script Date: 12/14/2016 18:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccessLevel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccessLevel] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_UserAccessLevel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserAccessLevel] ON
INSERT [dbo].[UserAccessLevel] ([Id], [AccessLevel], [Description]) VALUES (3, N'Admin', N'Website Administrators')
INSERT [dbo].[UserAccessLevel] ([Id], [AccessLevel], [Description]) VALUES (4, N'Users', N'Users can only view and AccessLeveladd comments to the website')
SET IDENTITY_INSERT [dbo].[UserAccessLevel] OFF
/****** Object:  Table [dbo].[UserAccount]    Script Date: 12/14/2016 18:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccount](
	[Id] [uniqueidentifier] NOT NULL,
	[RegistrationDate] [datetime] NULL,
	[Username] [nvarchar](10) NULL,
	[Password] [nvarchar](max) NULL,
	[Salt] [nvarchar](max) NULL,
	[AccessLevel] [int] NULL,
 CONSTRAINT [PK_UserAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'd4f64110-b752-4278-ae7e-0107d5b33584', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'c34b180a-2578-4cdd-b2c8-016511cb4e94', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'30d0890c5a3d127036a44df4ed50e007fc8f78f1ef7a6015175d7fae967919ee', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'b8a5a0fd-0c07-4cd8-bc07-087eec4a6107', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'30d0890c5a3d127036a44df4ed50e007fc8f78f1ef7a6015175d7fae967919ee', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'f2f9a79a-b8be-4edd-88af-08daaf74c843', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'd18f3bd5-3906-44f1-bfb4-12b7b5e4f297', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'9b5a9921-a932-46f2-be0f-12e798ba7aec', CAST(0x0000A6DC00000000 AS DateTime), N'jin', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'98af6295-9dee-402a-bebe-14ebccf5d450', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'62831ac1-831d-4876-80e7-2973aa0cf713', CAST(0x0000A6DC00000000 AS DateTime), N'asdfasf', N'a9508b78d22511c5ccdf745da28e77ea1b78930980ebacd78aa024ffb720ddc3', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'423fa09c-3cb3-4871-bbe8-2aef134745db', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'90c9cce3-18aa-4eec-a792-2e756f2acd01', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'3962fac2-20a4-447a-bbfb-3d6c576d908f', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'c701286d7c3c34e5e4f4503541579c24ef5064c65d392a0ebbaa78d819d9d276', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'd61d6c52-a49e-4602-8469-62db97b56756', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'c701286d7c3c34e5e4f4503541579c24ef5064c65d392a0ebbaa78d819d9d276', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'5236ba34-5718-4a6e-bd3b-6b83aa719b54', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'3f899777-27d5-4ea0-816f-73e02d4bf54b', CAST(0x0000A6DC00000000 AS DateTime), N'jin29n', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'c6e9b5d0-773d-4c0c-9a5d-8e398e2baf78', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'0c74981b-5923-43d6-b172-8fbffb9a1e66', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'8609c1b2-cd41-4fb0-a2c9-97060e1786a2', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'14084c66-637f-4c12-9fb8-9ec04c7491a8', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'19fef7db-7a6e-474f-bb6a-a23ffddf6c29', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'c701286d7c3c34e5e4f4503541579c24ef5064c65d392a0ebbaa78d819d9d276', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'f10c9bc6-472c-483d-834b-a31e1cf9454e', CAST(0x0000A6DC00000000 AS DateTime), N'asdfasf', N'183c1b634da0078fcf5b0af84bdcbb3e817708c3f22b329be84165f4bad1ae48', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'72360793-a364-4a04-b8f2-a54baa0bab51', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'c19f7fbe-af58-4324-bc18-ac006a92713a', CAST(0x0000A6DC00000000 AS DateTime), N'asdf', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'1084b889-c176-4138-b477-b2680ef364e5', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'3d8825671125e9bfddbf5487f4202c6292552f2b59a8b4d2811559781fe27250', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'e0b1e351-30dc-41ec-b224-bbeecab227d1', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'30d0890c5a3d127036a44df4ed50e007fc8f78f1ef7a6015175d7fae967919ee', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'd9fb2efc-327d-46ab-a021-cc58a5ae87e9', CAST(0x0000A6DC00000000 AS DateTime), N'asdasd', N'90225a8293afd8c85b7a838bfc6cab76026a5c456fbeb67f850fe82901689cdc', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'6e3c6487-ca05-424d-b9f6-ccecd174e499', CAST(0x0000A6DC00000000 AS DateTime), N'jin29neci', N'30d0890c5a3d127036a44df4ed50e007fc8f78f1ef7a6015175d7fae967919ee', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'6474a952-5f01-42b7-803a-d3eec0afbb44', CAST(0x0000A6DC00000000 AS DateTime), N'jin', N'183c1b634da0078fcf5b0af84bdcbb3e817708c3f22b329be84165f4bad1ae48', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'4ffacc01-1a10-492b-bccc-f3f6086c10a9', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'c701286d7c3c34e5e4f4503541579c24ef5064c65d392a0ebbaa78d819d9d276', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'b2d8dcae-a9cf-443c-8397-f5a4635a0fd6', CAST(0x0000A6DC00000000 AS DateTime), N'ascom', N'54a1205c0976e837328cd7b92c9d841a8254f7d55b4b206482be44e1610920fc', N'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'2cac747d-45ec-42a1-bb52-f71e0c3b491a', CAST(0x0000A6DC00000000 AS DateTime), N'as', N'c701286d7c3c34e5e4f4503541579c24ef5064c65d392a0ebbaa78d819d9d276', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
INSERT [dbo].[UserAccount] ([Id], [RegistrationDate], [Username], [Password], [Salt], [AccessLevel]) VALUES (N'6ec65e25-ffd4-4c42-9f28-f95ad91ac554', CAST(0x0000A6DC00000000 AS DateTime), N'jin', N'183c1b634da0078fcf5b0af84bdcbb3e817708c3f22b329be84165f4bad1ae48', N'b5d4045c3f466fa91fe2cc6abe79232a1a57cdf104f7a26e716e0a1e2789df78', 4)
/****** Object:  StoredProcedure [dbo].[usp_UserAccount_NewAccount_Registration]    Script Date: 12/14/2016 18:09:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UserAccount_NewAccount_Registration]
	@AffectedRows as int out,
	@RegistrationDate as datetime,
	@Username as nvarchar(10),
	@Password as nvarchar(max),
	@Salt as nvarchar(max),
	@AccessLevel as int
AS
BEGIN
	
	DECLARE @Id as uniqueidentifier 
	
	SET @Id = NEWID()
	
	INSERT INTO [dbo].UserAccount(Id,RegistrationDate,Username,Password, Salt, AccessLevel) VALUES (@Id, @RegistrationDate, @Username, @Password, @Salt, @AccessLevel)
	
	SET @AffectedRows = @@ROWCOUNT
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_UserAccount_DoesUserExists]    Script Date: 12/14/2016 18:09:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UserAccount_DoesUserExists]
@Username as nvarchar(10)
as
BEGIN
	SELECT COUNT(*) FROM UserAccount WHERE Username = @Username
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Authentication]    Script Date: 12/14/2016 18:09:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_User_Authentication]
	-- Add the parameters for the stored procedure here
	@Username as nvarchar(10),
	@Password as nvarchar(max)
AS
BEGIN
	
	SELECT * FROM UserAccount WHERE Username = @Username AND Password = @Password
	
END
GO
/****** Object:  ForeignKey [FK_UserAccount_UserAccessLevel]    Script Date: 12/14/2016 18:09:08 ******/
ALTER TABLE [dbo].[UserAccount]  WITH CHECK ADD  CONSTRAINT [FK_UserAccount_UserAccessLevel] FOREIGN KEY([AccessLevel])
REFERENCES [dbo].[UserAccessLevel] ([Id])
GO
ALTER TABLE [dbo].[UserAccount] CHECK CONSTRAINT [FK_UserAccount_UserAccessLevel]
GO
