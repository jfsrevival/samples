<%
Class UserAccount

Private m_Id

Public Property Get Id
    Id =  m_Id
End Property

Public Property Let Id(value)
    m_Id = value
End Property

'---------------RegistrationDate--------------'

Private m_registrationDate

Public Property Get RegistrationDate 
    RegistrationDate = m_registrationDate
End Property

Public Property Let RegistrationDate(value)
    m_registrationDate = value 
End Property 

'--------------End of RegistrationDate-----------'

'---------------UserName-----------------'
Private m_userName  

Public Property Get UserName 
    UserName = m_userName
End Property

Public Property Let UserName(value)
    m_userName = value 
End Property 

'---------------End of UserName------------'

'---------------Password--------------'

Private m_password

Public Property Get Password 
    Password = m_password
End Property

Public Property Let Password(value)
    m_password = value 
End Property 

'---------------End of Password--------'

'------------------Salt----------------'

Private m_salt 

Public Property Get Salt 
    Salt =  m_salt
End Property

Public Property Let Salt(value)
    m_salt = value 
End Property 

'--------------End of Salt---------------'


Private m_accessLevel

Public Property Get AccessLevel
    AccessLevel = m_accessLevel
End Property

Public Property Let AccessLevel(value)
    m_accessLevel = value
End Property

End Class

Private m_loginSuccess



 %>