


<%

    Dim Connection
    Dim Command
    Dim RecordSet
    

    Function ValidateUserName(username)

        Set Connection = Server.CreateObject("ADODB.Connection")

        Connection.ConnectionString = "Provider=SQLOLEDB.1;Data Source=.\SQLEXPRESS;Initial Catalog=SimpleBlogDb;user id =sa;password=p@ssw0rd"

        Set Command = Server.CreateObject("ADODB.Command")

        Set RecordSet = Server.CreateObject("ADODB.RecordSet")

        Connection.Open
        
        With Command
            .ActiveConnection = Connection
            .CommandText  = "usp_UserAccount_DoesUserExists"
            .CommandType = 4
        End With

        Command.Parameter("@Username") = username
        
        Set RecordSet = Command.Execute

    End Function

    Function RegisterNewUser(user)

        Set Connection = Server.CreateObject("ADODB.Connection")
    
        Connection.ConnectionString = "Provider=SQLOLEDB.1;Data Source=.\SQLEXPRESS;Initial Catalog=SimpleBlogDb;user id =sa;password=p@ssw0rd"

        Set Command = Server.CreateObject("ADODB.Command")
        
        Connection.Open

        With Command
            .ActiveConnection = Connection
            .CommandText = "usp_UserAccount_NewAccount_Registration"
            .CommandType = 4
            .NamedParameters = True
        End With
      
        Set Parameter1 = Command.CreateParameter("@RegistrationDate",7,1,8,user.RegistrationDate) 
       
        Command.Parameters.Append Parameter1
        Command.Parameters.Append Command.CreateParameter("@Username",201,1, 10, user.UserName) 
        Command.Parameters.Append Command.CreateParameter("@Password",201,1,100, user.Password)  
        Command.Parameters.Append Command.CreateParameter("@Salt",201,1,1000, user.Salt)
        Command.Parameters.Append Command.CreateParameter("@AccessLevel",20,1,4,4)
        Command.Parameters.Append Command.CreateParameter("@AffectedRows",3,2,16)

	    Command.Execute 
            
             
        If (Command.parameters("@AffectedRows").value = 1)  Then 
            
            RegisterNewUser =1
        
        End If
    
        Set Command = Nothing
        
        Set Recordset = Nothing
        
        Connection.Close

        Set Connection = Nothing
  

    End Function

  



  


%>