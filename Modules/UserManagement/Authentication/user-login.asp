﻿<%

  Dim Con
  Dim Cmd
  Dim Rs
    
  Function AuthenticateUser(user)
    
    Dim LoggedInUser 

    Set LoggedInUser = new UserAccount

    Set Con = Server.CreateObject("ADODB.Connection")

    Con.ConnectionString = "Provider=SQLOLEDB.1;Data Source=.\SQLEXPRESS;Initial Catalog=SimpleBlogDb;user id =sa;password=p@ssw0rd"

    Set Cmd = Server.CreateObject("ADODB.Command")

    Set Rs = Server.CreateObject("ADODB.RecordSet") 

    Con.Open
   
    With Cmd
    
        .ActiveConnection = Con
        .CommandText = "usp_User_Authentication"
        .CommandType = 4

    End With

    Cmd.Parameters("@Username") = user.UserName
    Cmd.Parameters("@Password") = user.Password

   Set Rs = Cmd.Execute
   Set Cmd = Nothing

    If Not Rs.EOF Then 
        Set LoggedInUser = Nothing
        Set LoggedInUser = new UserAccount
        LoggedInUser.Id = Rs(0)
        LoggedInUser.UserName = Rs(2)
        LoggedInUser.AccessLevel = Rs(5)
        
        Set AuthenticateUser = LoggedInUser
    Else 
        Set AuthenticateUser = user

    End If

   Rs.Close
   
   Con.Close 


  End Function  

%>