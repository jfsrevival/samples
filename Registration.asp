<%@  language="VBScript" %>

<%
    Response.Expires=-1
    Response.CacheControl="no-cache"
%>

<!--#include file="Modules/UserManagement/Registration/user.asp"-->
<!--#include file="Modules/UserManagement/Registration/user-registration.asp"-->
<!--#include file="Modules/UserManagement/security/sha256.asp"-->
<!--#include file="Modules/UserManagement/Authentication/user-login.asp"-->
<!-- #include file="Modules/Config/config.asp"-->

<%
    Dim RegistrationSuccess 
    
        RegistrationSuccess = False

    If (Request.Form.Count > 0) Then

        Dim newUserAccount
        Dim loggingUser 

        Set newUserAccount = new UserAccount
        
        newUserAccount.RegistrationDate = Date
        newUserAccount.UserName = Request.form("username")
        newUserAccount.Salt = sha256(UserSalt)
        newUserAccount.Password = sha256(Request.form("password") & newUserAccount.Salt)
           
        If RegisterNewUser(newUserAccount) = 1 Then 
            
             RegistrationSuccess = True
             
            Set loggingUser = AuthenticateUser(newUserAccount)
             
            If loggingUser.AccessLevel Then
            
                Session("LoggedIn") = True
                Session("Username") = loggingUser.UserName
                Session("AccessLevel") = loggingUser.AccessLevel
                Session("Id") = loggingUser.Id

            End If

        End If

     End If
%>

<!--#include file="MasterPages/header.asp"-->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <% If RegistrationSuccess = False Then %>
                <div class="form-horizontal">
                    <form action="<%=Request.ServerVariables("URL")%>" method="post">

                        <div class="form-group">
                            <label class="control-label" for="username">Username:</label>
                            <input type="text" name="username" id="username" placeholder="Please input your username..." class="form-control" />
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:</label>
                            <input type="password" name="password" id="password" placeholder="Please input your password here..." class="form-control" />

                        </div>

                        <div class="form-group">
                            <label class="control-label" for="confirmpassword">Confirm Password: </label>
                            <input type="password" name="confirmpassword" id="confirmpassword" placeholder="Please confirm your password here..." class="form-control" />
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-info" />
                        </div>

                    </form>
                </div>
                <% Else %>

                <div id="registrationSuccess">

                    <p>
                        Please wait while you are being redirected. 
                        <img src="images/ajax-loader.gif" />
                        Thank you.
                    </p>
                    <p>If you think this doesn't redirect you please click <a href="blog.asp">here</a></p>
                </div>

                <%End If%>
            </div>
        </div>
    </div>
</div>

<!--#include file="MasterPages/footer.html"-->

<script>

    $(function(){

        var registrationSuccess = $("div#registrationSuccess");

        if(registrationSuccess.length != 0) {

            setTimeout(function () {        window.location.href= "Survey.asp"; }, 1000);
        }   
    });

</script>
