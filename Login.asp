﻿<%@  language="VBScript" %>

<%
    Response.Expires=-1
    Response.CacheControl="no-cache"
%>

<!--#include file="Modules/UserManagement/Authentication/user-login.asp"-->
<!--#include file="Modules/UserManagement/Registration/user.asp"-->
<!--#include file="Modules/UserManagement/security/sha256.asp"-->
<!-- #include file="Modules/Config/config.asp"-->

<%
    If (Request.Form.Count > 0) Then    

        Dim loggingUser 
         
        Set loggingUser = new UserAccount
        loggingUser.UserName = Request.Form("username")
        loggingUser.Salt = sha256(UserSalt)
        loggingUser.Password = sha256(Request.form("password") & loggingUser.Salt)
    
        Set loggingUser = AuthenticateUser(loggingUser)
            
        If loggingUser.AccessLevel Then
            
            Session("LoggedIn") = True
            Session("Username") = loggingUser.UserName
            Session("AccessLevel") = loggingUser.AccessLevel
            Session("Id") = loggingUser.Id
            
            Response.Redirect("survey.asp")

        End If
        
    End If
           
%>

<!--#include file="MasterPages/header.asp"-->

<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">

                <%If IsEmpty(Session("LoggedIn"))  Then%>

                <div class="row">

                    <form action="<%=Request.ServerVariables("URL")%>" method="post">

                        <div class="form-group">
                            <label class="control-label" for="username">Username: </label>
                            <input type="text" name="username" id="username" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Password: </label>
                            <input type="password" name="password" id="password" class="form-control" />
                        </div>
                        <div class="form-group">
                            
                            <input type="submit" class="btn btn-info" />
                        </div>

                    </form>

                </div>

                <%Else%>

                <div>
                    <h2>You are already loggedIn</h2>
                </div>

                <%End If%>
            </div>
        </div>
    </div>
</div>
<!--#include file="MasterPages/footer.html"-->
