﻿<!--#include virtual="MasterPages/header.asp"-->
<form id="question-form" action="#">
    <div>
        <h4>Exam Information</h4>
        <section>
            <div class="form-horizontal">
                <div>
                    <label class="control-label">Exam Name:</label>
                    <input type="text" id="examname" name="examname" class="form-control" />
                </div>
                <div>
                    <label class="control-label">Description:</label>
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
            </div>
        </section>
        <h4>Questions</h4>
        <section>

            <label>Click to add new question(s)</label>
            <button type="button" id="addQuestion">+</button>
            <h4><u>Survey Questions</u></h4>

            <div id="div1">

                <label id="itemnumber1" class="label-control">Question #<span>1.</span></label>
                <input type="text" id="question1" name="question1" class="form-control" />
                <a href="#" id="remove"><span class="glyphicon glyphicon-remove-circle"></span></a>

            </div>


        </section>
        <h4>Answers</h4>
        <section>

        </section>
    </div>
</form>

<!--#include virtual="MasterPages/footer.html"-->
<script src="../Scripts/jquery.steps.min.js"></script>
<script>

    $(function(){
        
        var form = $("#question-form");
        
        if(form){
             
            form.children("div").steps({
                    headerTag: "h4",
                    bodyTag: "section",
                    transitionEffect: "slideLeft"
            });
        }
    });

    function processElement(element)
    {
        
    }

    $("body").on("click", "a#remove", function(e) {

        e.preventDefault();
        
        var $this = $(this);

    });

    $("body").on("click", "button#addQuestion", function(e) {
    
           var $this = $(this),
               divElement =  $("div[id*='div']").last(),
               length = $("div[id*='div']").size(),
               clone = divElement.clone(),
               div,
               label,
               span,
               text;

        if(length <=5){

            clone.attr("id" , "div" + (length +1));
                
            label = clone.find("label#itemnumber" + length);
            label.attr({"id": "itemnumber" + (length + 1), "name": "itemnumber" + (length + 1) });
            span = label.find("span");
            span.text((length +1));
            
            text = clone.find("input#question" + length);
            text.attr( {"id" : "question" + (length + 1), "name"  : "question" + (length + 1)});        
            text.val("");

           clone.insertAfter(divElement);
        }

        if(length ==5){
            $this.attr("disabled", true);
        }

    });

</script>
